<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromoController extends Controller
{
    public static function execute($params)
    {
        $text = "";
        if ($params[1] === 'create') {
            if ($params[2] === 'longstay') {
                $text = self::createPromoLongstay($params, $text);
            } else if ($params[1] === 'periodic') {
                $text = self::createPromoPeriodic($params, $text);
            }
        }

        return $text;
    }

    private static function createPromoPeriodic($params, $text)
    {
        return $text;
    }

    private static function createPromoLongstay($params, $text)
    {
        return $text;
    }
}
