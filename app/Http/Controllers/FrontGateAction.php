<?php

namespace App\Http\Controllers;

use App\Http\Service\MessageFetcherService;
use App\Http\Service\Sender\MessageSenderServices;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FrontGateAction extends Controller
{
    private $fetcherService;
    private $senderService;

    public function __construct(
        MessageFetcherService $fetcherService,
        MessageSenderServices $senderService
    )
    {
        $this->fetcherService = $fetcherService;
        $this->senderService = $senderService;
    }

    public function __invoke()
    {
        $telegramData = json_decode(file_get_contents('php://input'), TRUE);

        $fetchedMessage = $this->fetcherService->getMessage($telegramData);

        $chatId = $fetchedMessage['id'];
        $text = $fetchedMessage['text'];

        $data = explode(' ', $text);

        switch (strtolower($data[0])) {
            case '/room':
                $execute = ListAllRoomTypeFromBuildingIdAction::execute($data);
                $this->senderService->sendMessage($execute, $chatId);
                break;
            case '/building':
                $execute = BuildingController::execute($data);
                $this->senderService->sendMessage($execute, $chatId);
                break;
            case '/promo':
                $execute = PromoController::execute($data);
                $this->senderService->sendMessage($execute, $chatId);
                break;
            default:
                $this->senderService->sendHelpMessage($chatId);
                break;
        }
    }
}
