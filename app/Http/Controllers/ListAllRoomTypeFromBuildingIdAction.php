<?php

namespace App\Http\Controllers;

use App\Models\Building;
use App\Models\Repositories\RoomType\RoomTypeRepositoryInterface;
use App\Models\Room;
use App\Models\RoomType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ListAllRoomTypeFromBuildingIdAction extends Controller
{
    /**
     * /room status room_type 406
     * /room status building 37
     *
     * @param $params
     * @return string
     */
    public static function execute($params)
    {
        $text = "";
        if ($params[1] === 'status') {
            $data = null;
            $rooms = null;
            if ($params[2] === 'building') {
                $data = Building::find($params[3]);
                $text .= "*$data->build_name* \n";
                $roomTypes = $data->roomTypes()->get()->map(function ($roomType) {
                    return $roomType->roomtype_id;
                });

                $rooms = Room::whereIn('rooms_type_id', $roomTypes)->get();
            } else if ($params[2] === 'room_type') {
                $data = RoomType::find($params[3]);
                $text .= "*$data->roomtype_name* \n";
                $rooms = $data->rooms()->get();
            }

            $rooms->each(function ($room) use (&$text) {
                $text .= "$room->rooms_id - $room->rooms_name - " .
                    ($room->rooms_del_status === '0' ? "published" : "unpublished") . " - " .
                    ($room->rooms_status_id === 1 ? 'active' : 'inactive') .
                    "\n";
            });
        } else if ($params[1] === 'set') {
            $status = $params[3] === 'active' ? 1 : 6;
            DB::table('rooms')->where('rooms_id', $params[2])->update([
                'rooms_status_id' => $status
            ]);

            $text .= "$params[2] set to $params[3]";
        }

        return $text;
    }
}
