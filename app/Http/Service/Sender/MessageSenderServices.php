<?php


namespace App\Http\Service\Sender;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class MessageSenderServices
{
    private $client;
    private $token;
    private $url;
    private $query = [];

    public function __construct()
    {
        $this->client = new Client();
        $this->token = config('app.bot_token');
        $this->url = "https://api.telegram.org/bot$this->token/sendMessage";
    }

    public function sendMessage($message, $chatId)
    {
        Log::info(json_encode($this->query));
        $this->client->request('GET', $this->url, [
            'query' => [
                'chat_id'    => $chatId,
                'text'       => $message,
                'parse_mode' => "markdown"
            ]
        ]);
    }

    public function sendHelpMessage($chatId)
    {
        $message = "BANTUAN \n\n" .
            "`/room set {id} {status}` \n" .
            "- status can be *active* or *inactive* \n\n\n" .
            "`/room status {type} {id}` \n" .
            "- type can be *building* or *room_type*";

        $this->client->request('GET', $this->url, [
            'query' => [
                'chat_id'    => $chatId,
                'text'       => $message,
                'parse_mode' => "markdown"
            ]
        ]);
    }
}
