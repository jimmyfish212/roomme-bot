<?php


namespace App\Http\Service;


class MessageFetcherService
{
    public function getMessage($message) : array
    {
        /**
         * 0 - original default message
         * 1 - original photo message with caption
         * 2 - edited default message
         * 3 - not categorized / error message type / Invalid command.
         */
        $messageType = $this->detectMessageType($message);
        $result = null;

        switch ($messageType) {
            case 0:
                $result = [
                    'text' => $message['message']['text'],
                    'id' => $message['message']['chat']['id'],
                ];

                break;
            case 1:
                $result = [
                    'text' => $message['message']['caption'],
                    'id' => $message['message']['chat']['id'],
                ];

                break;
            case 2:
                $result = [
                    'text' => $message['edited_message']['text'],
                    'id' => $message['edited_message']['chat']['id'],
                ];

                break;
            case 3:
                $result = [
                    'text' => null,
                    'id' => null,
                ];

                break;
            default:
        }

        return $result;
    }

    public function detectMessageType($message)
    {
        $result = null;

        if (isset($message['message'])) {
            if (isset($message['message']['photo'])) {
                if (isset($message['message']['caption'])) {
                    $result = 1;
                } else {
                    $result = 3;
                }
            } else {
                if (isset($message['message']['text'])) {
                    $result = 0;
                } else {
                    $result = 3;
                }
            }
        } elseif (isset($message['edited_message'])) {
            if (isset($message['edited_message']['text'])) {
                $result = 2;
            } else {
                $result = 3;
            }
        }

        return $result;
    }
}
